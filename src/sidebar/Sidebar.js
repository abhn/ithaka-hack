import React, { Component } from 'react'
import TourCard from '../tourCard/TourCard'
import './Sidebar.css'

class SideBar extends Component {

  constructor() {
    super()
    this.state = {
      tours: [],
      tourShowArr: [],
      searchQuery: ''
    }
    this.handleSearchQuery = this.handleSearchQuery.bind(this)
    this.onFabClick = this.onFabClick.bind(this)
    this.onCloseClick = this.onCloseClick.bind(this)
  }

  componentDidMount() {
    // get the tour data and store it in state
    fetch('http://dev.ithaka.travel/plan-hack/api/plans')
    .then((response) => {
      return response.json()
    })
    .then((json) => {
      this.setState({
        tours: json
      })
    })
    .catch((ex) => {
      console.log('parsing failed', ex)
    })

  }

  // helper function to get tour by its id
  getTourById(id) {
    const tours = this.state.tours
    for(let i=0; i<tours.length; i++) {
      if(tours[i]._id === id) {
        return tours[i]
      }
    }
    return null
  }

  onFabClick(id) {
    // get the latitude and longitude and update mapsview
    const currTour = this.getTourById(id)
    const latLngArr = []
    let avgLat = 0
    let avgLng = 0

    for(let i=0; i<currTour.cities.length; i++) {
      avgLat += currTour.cities[i].lattitude
      avgLng += currTour.cities[i].longitude
      latLngArr.push(
        {
          position: {
            lat: currTour.cities[i].lattitude,
            lng: currTour.cities[i].longitude
          },
          key: Math.random(),
          defaultAnimation: 3
        }
      )
    }
    avgLat = avgLat / currTour.cities.length
    avgLng = avgLng / currTour.cities.length

    this.props.defaultCentreUpdate(avgLat, avgLng)
    this.props.mapUpdate(latLngArr)
  }

  onCloseClick(id) {
    // reset mapview with default lat and long
    const currTour = this.getTourById(id)
  }

  handleSearchQuery(event) {

    let searchQuery = event.target.value
    // search algorithm goes here!
    let tourShowArr = []
    for(let i in this.state.tours) {
      if(this.state.tours[i].planName.toLowerCase().indexOf(searchQuery.toLowerCase()) === -1) {
        tourShowArr[i] = 1
      } else {
        tourShowArr[i] = 0
      }
    }
    this.setState({tourShowArr})
  }

	render() {
    let tourList = []
    for (let tour in this.state.tours) {
      tourList.push(
        <div key={this.state.tours[tour]._id} className="card">
          <TourCard tour={this.state.tours[tour]} show={this.state.tourShowArr[tour]} onFabClick={this.onFabClick} onCloseClick={this.onCloseClick}/>
        </div>
      )
    }

		return (
      <div className="sidebar">
        <input type="text" placeholder="filter tours" value={this.state.value} onChange={this.handleSearchQuery}></input>
        <div className="sidebar-tour-list">
          {tourList}
        </div>
      </div>
		)
	}
}

export default SideBar
