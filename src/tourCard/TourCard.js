import React, { Component } from 'react'
import './TourCard.css'

class TourCard extends Component {

  constructor() {
    super()
    this.onFabClick = this.onFabClick.bind(this)
    this.onCloseClick = this.onCloseClick.bind(this)
  }

  onFabClick(id) {
    this.props.onFabClick(id)
  }

  onCloseClick(id) {
    this.props.onCloseClick(id)
  }

	render() {
    let cityArr = []
    for(let i=0; i<this.props.tour.cities.length; i++) {
      cityArr.push(
        <li key={Math.random()}><b>{this.props.tour.cities[i].cityTitle}</b> - {this.props.tour.cities[i].cityDuration}</li>
      )
    }

    let cityInfoArr = []
    for(let i=0; i<this.props.tour.cities.length; i++) {
      cityInfoArr.push(
        <div key={Math.random()}>
          <h4>{this.props.tour.cities[i].cityTitle}</h4>
          <p>{this.props.tour.cities[i].cityDescription}</p>
        </div>
      )
    }

		return (
      <div>
        {/* this will decide if the card is visible */}
        {this.props.show !== 1 ? (
          <div className="card">

            <div className="card-image">
              <img className="activator" src={this.props.tour.planImageUrl}/>
              <span className="card-title">{this.props.tour.planName.slice(0, 60)}</span>
              <a className="btn-floating activator halfway-fab waves-effect waves-light red" onClick={this.onFabClick.bind(this, this.props.tour._id)}><i className="material-icons">add</i></a>
            </div>

            <div className="card-content">
              <p>{this.props.tour.planDescription}</p>
            </div>

            <div className="card-action">
              <p><b>Plan Duration</b> {this.props.tour.planDuration}</p>
              <ul>
                {cityArr}
              </ul>
            </div>

            <div className="card-reveal">
              <span className="card-title grey-text text-darken-4">Mini City Tour!<i className="material-icons right" onClick={this.onCloseClick.bind(this, this.props.tour._id)}>close</i></span>
              {cityInfoArr}
            </div>

          </div>
        ) : (<div></div>) }
      </div>

    )
	}
}

export default TourCard
