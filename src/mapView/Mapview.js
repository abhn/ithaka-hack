import React, { Component } from 'react'
import './Mapview.css'
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import _ from "lodash";
import Helmet from "react-helmet";

const GettingStartedGoogleMap = withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapLoad}
    zoom={props.defaultZoom}
    center={props.defaultCenter}
    onClick={props.onMapClick}
  >
    {props.markers.map(marker => (
      <Marker
        {...marker}
        onRightClick={() => props.onMarkerRightClick(marker)}
        key={Math.random()}
      />
    ))}
  </GoogleMap>
));

class Mapview extends Component {

  constructor() {
    super()
    this.state = {
      markers: [],
      defaultZoom: 4,
      defCenter: [3.94, 108.73]
    }
  }

  componentWillReceiveProps(nextProps) {
    const latLngArr = nextProps.latLngArr
    const defCenLat = nextProps.defCenter[0]
    const defCenLng = nextProps.defCenter[1]
    this.setState({
      defaultZoom: 5,
      markers: latLngArr,
      defCenter: [defCenLat, defCenLng]
    })
  }

  handleMapLoad = this.handleMapLoad.bind(this);
  handleMapClick = this.handleMapClick.bind(this);
  handleMarkerRightClick = this.handleMarkerRightClick.bind(this);

  handleMapLoad(map) {
    this._mapComponent = map;
    if (map) {
      console.log(map.getZoom());
    }
  }

  /*
   * This is called when you click on the map.
   * Go and try click now.
   */
  handleMapClick(event) {

  }

  handleMarkerRightClick(targetMarker) {

  }

  render() {
    console.log(this.state)
    return (
      <div style={{height: `500px`}} className="mapview">
        <Helmet
          title="Ithaka Hack"
        />
        <GettingStartedGoogleMap
          defaultZoom={this.state.defaultZoom}
          defaultCenter={{ lat: this.state.defCenter[0], lng: this.state.defCenter[1] }}
          containerElement={
            <div style={{ height: `100vh` }} />
          }
          mapElement={
            <div style={{ height: `100vh` }} />
          }
          onMapLoad={this.handleMapLoad}
          onMapClick={this.handleMapClick}
          markers={this.state.markers}
          onMarkerRightClick={this.handleMarkerRightClick}
        />
      </div>
    );
  }
}
export default Mapview
