import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import Mapview from './mapView/Mapview'
import Sidebar from './sidebar/Sidebar'
import TourCard from './tourCard/TourCard'

class App extends Component {

  constructor() {
    super()
    this.state = {
      latLngArr: [],
      defCenLat: 3.94,
      defCenLng: 108.73
    }
    this.mapUpdate = this.mapUpdate.bind(this)
    this.defaultCentreUpdate = this.defaultCentreUpdate.bind(this)
  }

  componentDidMount() {
    // initialize maps at 0,0
    let latLngArr = [
      {
        latitude: 3.94,
        longitude: 108.73
      }
    ]
    this.setState({
      latLngArr
    })
  }

  mapUpdate(latLngArr) {
    this.setState({
      latLngArr
    })
  }

  defaultCentreUpdate(lat, lng) {
    this.setState({
      defCenLat: lat,
      defCenLng: lng
    })
  }

  render() {
    return (
      <div className="row">
        <div className="col s3">
          <Sidebar mapUpdate={this.mapUpdate} defaultCentreUpdate={this.defaultCentreUpdate}/>
        </div>
        <div className="col s9">
          <Mapview latLngArr={this.state.latLngArr} defCenter={[this.state.defCenLat, this.state.defCenLng]}/>
        </div>
      </div>
    )
  }
}

export default App;
